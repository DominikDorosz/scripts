**Developer Notes**
================================================================================

- When running scripts, place them in /etc/fitwiz/scripts directory.
- We are using ubuntu to develop, therefore, to run everything properly
  we need at least ubuntu version 14.04 or higher.
