import RPi.GPIO as GPIO
from lib_nrf24 import NRF24
import time
import spidev
import socket
import struct
import timeit
import json

pairingMode = 2
connectedSensors = {}



GPIO.setmode(GPIO.BCM)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
TCP_IP = '127.0.0.1'
TCP_PORT = 9899
BUFFER_SIZE = 1024

s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn,addr = s.accept()


print 'Connection address: ', addr

rx_pipe = [0xF0, 0xF0, 0xF0, 0xF0, 0xE1]

radio = NRF24(GPIO, spidev.SpiDev())
radio.begin(1,17)

radio.setPayloadSize(32)
radio.setChannel(0x76)
radio.setDataRate(NRF24.BR_1MBPS)
radio.setPALevel(NRF24.PA_MIN)

radio.setAutoAck(True)
radio.enableDynamicPayloads()
radio.enableAckPayload()

radio.openReadingPipe(1, rx_pipe)
radio.printDetails()
radio.startListening()
        


while(1):

##    receivedJSONdata = json.loads(conn.recv(BUFFER_SIZE))
##    if not data:break

    
    if pairingMode == 2:
        pairCounter = 0 
        pairingMode = 1
    
    
    while not radio.available(0):
        time.sleep(1/100)


    recv_buffer = []
    radio.read(recv_buffer, radio.getDynamicPayloadSize())
    header = recv_buffer[0:3]
    payload = recv_buffer[3:len(recv_buffer)]
##    print("Received SensorID: {}" . format(header[0]))
##    print("Received SequenceNumber: {}" . format(header[1]))
##    print("Received MessageType: {}" . format(header[2]))
##    print("Received message: " + format(payload[1],"02x") + format(payload[0],"02x"))

    receivedMessage = format(payload[1],"02x") + format(payload[0],"02x")



    ## exit pairing mode if timer runs out and send JSON message
    if pairingMode == 1:
        
        pairCounter = pairCounter + 1

        ## save all connected Devices
        if header[2] == 2:

            writePipe = [0xF0, 0xF0, 0xF0, payload[1], payload[0]]
            print(writePipe)
            time.sleep(1/100)
                        
            radio.stopListening()
            radio.openWritingPipe(writePipe)
            message = [header[0],0,3,0,100]

            for count in range(1,8):
                radio.write(message)
                
            
            radio.startListening()

            connectedSensors[header[0]] = receivedMessage
            
        if pairCounter > 200:
            
            pairingMode = 0
            pairCounter = 0
            JSONconnectedSensors = '{"jsonrpc": "2.0", "method":"pair_response", "params": {'+ str(connectedSensors) +'}}'

            conn.send(JSONconnectedSensors)
            print(str(JSONconnectedSensors))
            
            
    ## muscle_activation data
    else:
        
        if header[0] in connectedSensors:
            JSONsensorData = '{"jsonrpc": "2.0", "method":"property_changed", "params": {"sensor_id": "' + str(header[0]) + '", "sequence_number": "' + str(header[1]) + '", "Muscle_activation_val": "' + receivedMessage + '"}}'

            conn.send(JSONsensorData)
            print(str(JSONsensorData))



conn.close()
