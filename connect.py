#!/usr/bin/env python

import socket
import time
#import json
import sys

#from PyQt5 import QtCore

TCP_IP = 'localhost'
TCP_PORT = 9898
BUFFER_SIZE = 1024


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()
print 'Connection address: ', addr

time.sleep(2)
conn.send('{"jsonrpc": "2.0", "method":"property_changed", "params": {"sensor_id": "a1b2", "connection_status": "connected"}}')

heart_rate_high = True
is_incrementing = True
while 1:
    data = conn.recv(BUFFER_SIZE)
    if not data:break
    #print "received data: ", data
    if is_incrementing == True:
        for i in range(50, 100):
            data = '{"jsonrpc":"2.0", "method":"property_changed", "params": {"muscle_activation": %s}}' % i
            conn.send(data)
            time.sleep(0.3)


    #conn.send(data) #echo
conn.close()
